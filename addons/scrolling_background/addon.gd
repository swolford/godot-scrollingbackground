tool
extends EditorPlugin

# Add our custom node type

func _enter_tree():
    # Initialization of the plugin goes here
	add_custom_type("ScrollingBackground2D", "Node", preload("scrolling_background.gd"), preload("icon.svg"))

# Remove the custom node type

func _exit_tree():
    # Clean-up of the plugin goes here
    remove_custom_type("ScrollingBackground2D")