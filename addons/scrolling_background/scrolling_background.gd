tool
extends Node

# Inspector properties.
export var texture : Texture = null setget set_texture
export var z_index : int = 0
export var scale : Vector2 = Vector2.ONE
export var modulate : Color = Color.white setget set_modulate
export var speed : Vector2 = Vector2.LEFT
export var run_on_physics_process : bool = false

# Private properties.
var _screen_size : Vector2
var _background : Sprite

func _ready() -> void:
	_refresh_child()

func _process(delta : float) -> void:
	if not run_on_physics_process:
		_refresh(delta)

func _physics_process(delta : float) -> void:
	if run_on_physics_process:
		_refresh(delta)

func _refresh(delta : float) -> void:
	if _background == null or texture == null:
		_refresh_child()
		return
		
	var current_position : Vector2 = Vector2(_background.position.x + (speed.x * delta), _background.position.y + (speed.y * delta))
	
	if (current_position.x < 0 - texture.get_width() * scale.x * 2) or (current_position.x > 0):
		current_position.x = 0 - texture.get_width() * scale.x
	
	if (current_position.y < 0 - texture.get_height() * scale.y * 2) or (current_position.y > 0):
		current_position.y = 0 - texture.get_height() * scale.y
	
	_background.position = current_position

# Update the Background node based on settings.
func _refresh_child() -> void:
	if texture == null or get_viewport() == null:
		# Texture not set or we don't have a viewport, return early.
		return
		
	if _background == null:
		# Background node not set, create one and add it to the tree.
		_background = Sprite.new()
		_background.set_name("Background")
		add_child(_background)
	
	# Get our screen size.
	_screen_size = get_viewport().get_visible_rect().size
	
	if _background:
		# The background node exists, update its texture and modulation.
		_refresh_texture()
		_refresh_modulate()
		
		# Update the background node's position and z-index.
		var current_position : Vector2 = _background.position
		current_position.x = 0 - texture.get_width() * scale.x
		current_position.y = 0 - texture.get_height() * scale.y
		_background.position = current_position
		_background.z_index = z_index

# Update the Background node's modulation based on settings.
func _refresh_modulate() -> void:
	if _background != null and modulate != null:
		_background.modulate = modulate

# Update the Background node's texture based on settings.
func _refresh_texture() -> void:
	if _background != null:
		_background.texture = texture
		
		if texture:
			# Set the texture to repeat if one is defined.
			texture.set_flags(texture.get_flags() | Texture.FLAG_REPEAT)
			
			# Set the size of the texture. We are using 2x the size of the screen, scaled.
			var region_rect : Rect2 = Rect2(0, 0, ceil(_screen_size.x / scale.x) + texture.get_width() * 2, ceil(_screen_size.y / scale.y) + texture.get_height() * 2)
			# Configure the rest of the background node's parameters.
			_background.region_enabled = true
			_background.region_rect = region_rect
			_background.centered = false
			_background.scale = scale

func set_texture(value):
	texture = value
	_refresh_texture()

func set_modulate(value):
	modulate = value
	_refresh_modulate()
